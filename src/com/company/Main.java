package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        Ввести n строк с консоли, найти самую короткую строку. Вывести эту строку и ее длину.
        Scanner scan = new Scanner(System.in);
        System.out.println("введите количество строк, которое Вы желаете ввести: ");
        int numRows = Integer.parseInt(scan.nextLine());

        String[] arrayOFRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.printf("Введите строку %d: ", i + 1);
            String row = scan.nextLine();
            arrayOFRows[i] = row;
        }

        int minLength = arrayOFRows[0].length();
        String minLengthRow = arrayOFRows[0];

        for (int i = 0; i < numRows; i++) {
            if (minLength > arrayOFRows[i].length()) {
                minLength = arrayOFRows[i].length();
                minLengthRow = arrayOFRows[i];
            }
        }
        System.out.println("Строка с наименьшим числом символов: " + minLengthRow);
        System.out.println("Длинна строки: " + minLengthRow.length());

    }
}
